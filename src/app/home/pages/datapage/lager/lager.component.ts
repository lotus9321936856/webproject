import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {LagerService} from '../../../../services/lager.service';
import {Beer} from './model/Beer';
import {Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatSort} from '@angular/material/sort';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-lager',
  templateUrl: './lager.component.html',
  styleUrls: ['./lager.component.css']
})
export class LagerComponent implements OnInit, AfterViewInit {
  dataSource = new MatTableDataSource();
  displayedColumns = ['id', 'image', 'name', 'abv', 'ibu', 'style', 'ounces'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public pageSize = 10;
  pageEvent: PageEvent;
  public currentPage = 0;
  public totalSize = -1;
  images: any[];
  public array: any;

  constructor(private lagerService: LagerService,
              private ref: ChangeDetectorRef,
              @Inject(DOCUMENT) private document: Document,
              private router: Router,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.loadProductsPage();
    this.loadImages();
  }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  filterData(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  loadProductsPage() {
    this.lagerService.getLager().subscribe(res => {
        this.dataSource = new MatTableDataSource<Beer>(res)
        this.dataSource.paginator = this.paginator;
        this.array = res;
        this.totalSize = this.array.length;
        this.iterator();
        this._snackBar.open('Data Loaded Successfully', '', {
          duration: 2000,
        });
      },
      error => {
        this._snackBar.open('Something went wrong', '', {
          duration: 4000,
        });
      });
  }

  loadImages()  {
    this.images = this.lagerService.getImages()
    this.lagerService.getImageData().subscribe(res => {
      this.images = res as Beer[]
      this._snackBar.open('Data Loaded Successfully', '', {
        duration: 2000,
      });
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    this.dataSource = new MatTableDataSource<Beer>(this.array.slice(start, end)) ;
  }
  goToUrl(): void {
    this.document.location.href = 'https://gitlab.com/lotus9321936856/webproject.git';
  }
}
