export class Beer {
  name: string;
  abv: string;
  ibu: string;
  style: string;
  ounce: string;
}
