import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSliderModule} from '@angular/material/slider';
import {MatChipsModule} from '@angular/material/chips';
import {MatTableModule} from '@angular/material/table';
@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule, MatCheckboxModule, MatSelectModule, MatDialogModule, MatSliderModule, MatChipsModule, MatIconModule, MatTableModule,
    FormsModule, ReactiveFormsModule,
  ],
  exports: [
    HeaderComponent
  ]
})
export class LayoutModule { }
