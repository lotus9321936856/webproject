import {ChangeDetectorRef, Component, Injectable, OnInit, TemplateRef} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {LagerService} from '../../../services/lager.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
@Injectable()

export class HeaderComponent implements OnInit {
  employeeForm: FormGroup;

  constructor(public dialog: MatDialog,
              private fb: FormBuilder,
              private api: LagerService,
              private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.employeeForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      mobile_number: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
    });
  }



  get controls() {
    return this.employeeForm.controls;
  }


  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.employeeForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
  addEmployee() {
     // this.api.addLager(this.employeeForm.value).subscribe(
     //   (res: any) => {
     //     if (res) {
     //       console.log('New customer added successfully');
     //     }
     //   },
     //   (error) => {
     //     console.log(error.error.message);
     //   }
     // );
  }

  openDialogWithTemplateRef(templateRef: TemplateRef<any>) {
    this.dialog.open(templateRef);
  }


}
