import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LagerService {

  constructor(private http: HttpClient) { }


  getLager(): Observable<any> {
    return this.http.get(environment.baseUrl + '/beercraft5bac38c.json');
  }

  getImageData(): Observable<any> {
    return this.http.get(environment.baseUrl + '/beerimages7e0480d.json');

  }

  getImages() {
    return [   {
      "image": "https://s3-ap-southeast-1.amazonaws.com/he-public-data/csm_01_02-2019_Beer_Brewing_53ef2818e58285158.png"

    },
      {
        "image": "https://s3-ap-southeast-1.amazonaws.com/he-public-data/Swedish_beerb2d62a0.jpg"
      },
      {
        "image" : "https://s3-ap-southeast-1.amazonaws.com/he-public-data/EVEREST_SPECIAL_LIMITED_EDITION_BEER_POKHARA_NEPAL_FEB_2013_28851073145129_201905131625260ec63f6.jpg"

      },
      {
        "image" : "https://s3-ap-southeast-1.amazonaws.com/he-public-data/https%20_specials-images.forbesimg.com_imageserve_5e325c56f133f400076b17b9_0x03b6f8ad.jpg"
      },
      {
        "image" : "https://s3-ap-southeast-1.amazonaws.com/he-public-data/low-calorie-beers-157981804958062d8.jpg"

      }
    ]
  }

}

